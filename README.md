# Onboarding

Welcome 👋 !

Below you can see all the steps required for your onboarding.

Feel free to create MR to update this file if you have some ideas to improve this file!

Thanks 🙏

# 💻 Browser

We try to use firefox since client also tests features using Firefox.

You can install firefox using brew:

`brew install --cask firefox`

# 📧 Email

Email details has been sent to you on your private mail.

- local client (Edison or AirMail)

https://www.edisonmail.com/

use following setup

## incoming mail (imap)

server: imap.zoho.com

port: 993 (SSL TLS)

## outgoing mail (smtp)

server: smtp.zoho.com

port: 465 (SSL TLS)

:warning: if you can't connect enable imap on your account:

```
1. Login to Zoho Mail
2. Go to Settings
3. Navigate to Mail Accounts and click the primary email address from the Mail accounts listing.
4. Under the IMAP section, check the IMAP Access checkbox.
5. IMAP access
6. Scroll down
7. click Save.
```

![enable imap](https://i.imgur.com/urlNY32.png)

# 📲 Slack - communicator for teams

- Invitation for Slack has been sent to @limebrains.com mail.

you can use browser based slack on
https://limebrains.slack.com

click `use Slack in your browser`

![slack use in browser](https://i.imgur.com/AEgOIYH.png)

- useful shortcuts

    - search across (omni search) - `cmd` + `k`

# 🦾 Gitlab Account

Credentials for Gitlab account has been sent to you via mail.

# 🧭 Top tracker

- Invitation for Slack has been sent to @limebrains.com mail.
- install via `brew install --cask toptracker` or manual download https://www.toptal.com/tracker

:warning: For description of current task please use link as full path to issue.
Example: https://code.mazars.global/mz/aaa/-/issues/9238


# 📲 Determine what to work on

Please check during the day:

1. email for messages/notifications
1. Slack for messages/notifications
1. Gitlab TODO on https://code.mazars.global/dashboard/todos
1. Gitlab issues assigned to you
1. verify with other team members or wojtek if you can work on that issue
1. if no task from above ask wojtek


# 💼 Working on issue workflow

```mermaid
graph TD
    A[Copy issue URL without '#'] --> B[Paste into Top Tracker]
    B --> C[Move issue to 'Doing']
    C --> D[Create a Merge Request using the provided button]
    D --> E[Git Fetch and Checkout Branch]
    E --> F[Read Description]
    F --> G[Ask Questions if Unclear]
    G --> H[Have a Plan]
    H -->|Commit Frequently| I[Commit at Least Once Per 30 min / 1h]
    I --> H
    H --> J[Issue Ready]
    J --> K[Record Video/Screenshot as Proof]
    K --> L[Add Proof to Issue as Comment]
    L --> M[Mark MR as Ready and change 'Assignee' in top right]
    M --> N[Change Status of Issue to 'Code Review' or 'Done Pending Review']
```

1. copy issue url (without '#')
1. paste into top tracker
1. move issue in doing
1. create a Merge Request using the provided button
1. git fetch to get the branch name and git checkout to change branch
1. read description
1. if not clear ask (team member or person that created)
1. have plan
1. commit at least once per 30min / 1h (atomic commits) as frequently as possible, even if not working fully
1. issue ready
1. record [video/screenshot](#screenshots--screen-recording) that proves that issue has been done
1. add it into issue as comment
1. mark MR as Ready and change 'Assignee' in top right
1. change status of issue to `Code Review` or `Done (pending review)`


## Screenshots & Screen recording

### Video (MacOS keyboard shortcut `cmd`+`shift`+`5`):
![qucicktime](https://i.imgur.com/PTJZEk1.jpeg)
![record video](https://i.imgur.com/AOxhpzh.png)

### Screenshot (MacOS keyboard shortcut `cmd`+`shift`+`4`)

#### mac2imgur
We recommend using `mac2imgur` to effortlessly upload screenshots to Imgur, simplifying the process of sharing images via URLs in issue tracking and communication. 

`brew install --cask mac2imgur`

# Bash setup

## 1. enable bash instead of zsh

`chsh -s /bin/bash`

## 2. useful bash_profile

https://gitlab.com/limebrains/bash_profile/-/raw/master/README.md

copy it into your `~/.bash_profile`

examples:

- `Get my public IP`

```
 06:33:25 _____________________________________________________________________________
| pythonicninja
|  ~/PycharmProjects/onboarding   (main)
| => myip
{
  "ip": "104.28.212.230",
  "city": "Bydgoszcz",
  "region": "Kujawsko-Pomorskie",
  "country": "PL",
  "loc": "53.1235,18.0076",
  "org": "AS13335 Cloudflare, Inc.",
  "postal": "85-001",
  "timezone": "Europe/Warsaw",
  "readme": "https://ipinfo.io/missingauth"
}
```

## 3. useful tips for shell

- history of commands

```bash
| pythonicninja
|  ~   (main)
| => history
 1507  bash_profile
 1508  history
```

- search historical commands

`control` + `r`

## 3. other info

🦅 Read more:
https://gitlab.com/limebrains/master-os

# 💻 SSH

## 1. Generate ssh-key

https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key

## 2. Add it to your gitlab account

https://code.mazars.global/-/profile/keys

## 3. Ssh config

Put the following config in .ssh/config file (create if it does not exist):

```
AddKeysToAgent yes

host *
    ForwardAgent yes
    GSSAPIAuthentication no
    StrictHostKeyChecking no

host code.mazars.global
   Hostname 52.169.52.102
```

# Setting up project

## git/config for fetching from code.mazars.global

`cat ~/.ssh/config`

```bash
AddKeysToAgent yes

host *
   ForwardAgent yes
   GSSAPIAuthentication no
   AddKeysToAgent yes
   ServerAliveInterval 15
   StrictHostKeyChecking no

host code.mazars.global
   Hostname 52.169.52.102
```

## 1. python

`brew update`

`brew install python@3.11`

change `@3.11` to version depending on the project

## 2. nvm

`curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash `

`nvm install 20`

`nvm use 20`

Version depends on the project

## 3. Python virtualenv

`python3 -m venv env`

Activate the environment with [alias from bash_profile](#2-useful-bash_profile)

`e`

or

`. env/bin/activate`

## 4. docker

1. install docker:
   - using brew cask: `brew install --cask docker`
   - or manual install: https://docs.docker.com/desktop/install/mac-install/

1. login into repository registry:
   - create an access token https://code.mazars.global/-/user_settings/personal_access_tokens
   - `docker login https://code-registry.mazars.global -u email@limebrains.com -p ACCESS_TOKEN`

1. stop all containers

   - `docker_stop` ([alias from bash_profile](#2-useful-bash_profile))

1. start all containers in docker-compose project:
   - `docker compose up -d` or `docker-compose up -d`

## 5. MongoDB tools

1. `brew tap mongodb/brew`

1. `brew install mongodb-database-tools`

### MongoDB GUI client

1. `brew install --cask robo-3t`

## 6. Iterm setup

use profile of hot key to have qucik access to shell for `cmd + .`:

[profile.json](./Hotkey Window.json)

![load-above-json-config-into-profiles](https://i.imgur.com/3nbQ3gt.png)

manual way:
https://pythonic.ninja/configuration-for-productive-terminal-on-osx.html

## 7. Itermocil

Automation of setting up your shells in iterm.

https://github.com/pythonicninja/itermocil

## 8. pipconf setup

generate token on https://code.mazars.global/-/user_settings/personal_access_tokens
select all scopes and insert it into `<token>` bellow and adjust `<email>` to match your account

create file inside your home dir:
```
| => cat ~/.pip/pip.conf
[global]
index= https://pypi.org/
index-url = https://pypi.org/simple
extra-index-url = https://<your_email>:<token>@code.mazars.global/api/v4/groups/1508/-/packages/pypi/simple
```

:warning: you can use same `<token>` as you used for docker login command

## 9. Project setup

Remove `dependency_links` and `extras_require` from pkg/setup.py and api/setup.py.

**Do not commit this change. Revert it after setup**

1. `cd ./pkg && make dev_install`
1. `cd ./api && make dev_install`
1. `cd ./web && make dev_install`

# 📝 Git

## 1. Aliases

https://gitlab.com/limebrains/master-git#useful-aliases

copy it into your git config `~/.gitconfig`

## 2. Commit message

```
short description of what you did,
https://link_to_issue_same_as_in_top_tracker
```

:warning: please don't add in this link any `#`

## 3. useful commands

- `git stash`
- `git ls`
- `git commit --amend` - you committed something with wrong message

## 4. Other info

🦅 Read more:
https://gitlab.com/limebrains/master-git


# 💻 Mac OS X useful shortcuts

- **Copy**: `Cmd` + `C`
- **Paste**: `Cmd` + `V`
- **Search (Spotlight)**: `Cmd` + `Space`
- **Cut**: `Cmd` + `X`
- **Undo**: `Cmd` + `Z`
- **Redo**: `Cmd` + `Shift` + `Z`
- **Save**: `Cmd` + `S`
- **Open**: `Cmd` + `O`
- **Close Window**: `Cmd` + `W`
- **Close All Windows**: `Cmd` + `Option` + `W`
- **Switch Between Apps**: `Cmd` + `Tab`
- **Force Quit an Application**: `Cmd` + `Option` + `Esc`
- **Emoji lookup**: `Cmd` + `Option` + `Space`

# 📋 Clipboard manager

https://github.com/aklein13/cp-clip/releases

- search - `cmd` + `shift` + `v`

# 🗒️ Notes

Use `Notatnik` which synced into your iCloud, so you can access it from phone or home PC.

# PyCharm

`brew install --cask pycharm`

useful plugins

- frame switcher https://plugins.jetbrains.com/plugin/7138-frame-switcher
- tabnine https://plugins.jetbrains.com/plugin/12798-tabnine-ai-code-completion--chat-in-java-js-ts-python--more

useful shortcuts

- omni search - press twice `shift`
- commit - `cmd` + `k`
- push - `cmd` + `shift` + `k`

:warning: if you need to clear pycharm due to it not working, here is script that clears all settings:
https://gitlab.com/PythonicNinja/jetbrains-reset-trial-evaluation-mac/-/raw/master/runme.sh

# TODO

- [ ] TODO: setup script = fast setup
- [ ] TODO: 
